#  Copyright (C) 2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
ARG VAULT_PLUGIN_VERSION=1.13.0
ARG KUSTOMIZE_VERSION=4.5.4
FROM curlimages/curl:7.78.0 as builder
ARG VAULT_PLUGIN_VERSION
ARG KUSTOMIZE_VERSION
RUN curl -L -o /tmp/argocd-vault-plugin https://github.com/IBM/argocd-vault-plugin/releases/download/v${VAULT_PLUGIN_VERSION}/argocd-vault-plugin_${VAULT_PLUGIN_VERSION}_linux_amd64
RUN curl -L  https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz | tar -zxvf - -C /tmp kustomize
RUN chmod +x /tmp/argocd-vault-plugin

FROM python:3.9-slim
USER root
RUN mkdir /vault-ca
RUN mkdir -p /home/argocd/cmp-server/config
COPY plugin.yaml /home/argocd/cmp-server/config/plugin.yaml
RUN chown -R 999 /home/argocd
COPY --from=builder /tmp/argocd-vault-plugin /usr/local/bin
COPY --from=builder /tmp/kustomize /usr/local/bin
RUN apt-get update || true && apt-get install -y python3-venv
RUN python3 -m venv /install
COPY kanod_updater /src/kanod_updater
COPY setup.py setup.cfg MANIFEST.in /src/
WORKDIR /src
RUN /install/bin/pip install .
USER 999
