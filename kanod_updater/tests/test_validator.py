#  Copyright (C) 2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import jsonschema  # type: ignore
from unittest import TestCase
import yaml

from .. import config_util
from .. import validator

eh = config_util.ErrorHandler()

load = yaml.safe_load

INFRA1 = '''
nexus:
  maven: https://1.1.1.1
  docker: 1.1.1.1:8888
controlPlaneFlavours: []
workerFlavours: []
addonFlavours: []
'''

CLUSTER1 = '''
controlPlane:
  flavour: default
  controlPlane:
    endPoint:
      host: 1.1.1.1
  serverConfig: {}
workers: []
addons: []
'''


class TestValidator(TestCase):

    def test_schema(self):
        schema = validator.get_schema()
        schema['$ref'] = '#/definitions/cluster_definition'
        jsonschema.Draft7Validator.check_schema(schema)

    def test_validate_infra(self):
        infra = load(INFRA1)
        try:
            validator.validate_infra(eh, infra)
        except Exception:
            pass
        self.assertEqual(eh.errors, [])

    def test_validate_cluster(self):
        cluster = load(CLUSTER1)
        try:
            validator.validate_cluster(eh, cluster)
        except Exception:
            pass
        self.assertEqual(eh.errors, [])
