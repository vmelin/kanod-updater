#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Dict, List, Optional, Union  # noqa: F401,H301

import logging

from . import config_util
from . import updater

log = logging.getLogger(__name__)


def baremetalpool_manifest(
    cluster: updater.Cluster, bmpool: config_util.YamlDict
    ) -> List[config_util.YamlDict]:

    size_calculated = int(cluster.controlplane["replicas"])
    for md in cluster.workers:
        size_calculated += int(md["replicas"])

    spec = {
        'size': bmpool.get('size', size_calculated),
        'poolname': bmpool.get('poolname', ""),
        'credentialName': bmpool.get('credentialName', ""),
        'address': bmpool.get('address', ""),
        'redfishschema': bmpool.get('redfishschema', ""),
    }

    bm_name = bmpool.get('name', "")

    manifest: config_util.YamlDict = {
        'kind': 'Baremetalpool',
        'apiVersion': updater.BAREMETALPOOL_GROUP,
        'metadata':
            cluster.metadata(f'{bm_name}'),
        'spec': spec
    }
    return [manifest]
