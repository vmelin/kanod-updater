#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Dict, Optional  # noqa: H301

from . import config_util
from . import updater


def cluster_manifest(cluster: updater.Cluster) -> config_util.YamlDict:
    '''Cluster-API manifest of the cluster'''

    networking = cast(
        config_util.YamlDict,
        cluster.k8sconfig.get('network', {}))
    cidr_services = networking.get('services', ['10.96.0.0/12'])
    cidr_pods = networking.get('pods', ['192.168.192.0/18'])
    service_domain = networking.get('domain', 'cluster-services.local')

    spec = {
        'clusterNetwork': {
            'services': {
                'cidrBlocks': cidr_services
            },
            'pods': {
                'cidrBlocks': cidr_pods
            },
            'serviceDomain': service_domain
        },
        'infrastructureRef': {
            'apiVersion': updater.CAPM3_GROUP,
            'kind': 'Metal3Cluster',
            'name': cluster.cluster_name
        },
        'controlPlaneRef': {
            'apiVersion': updater.CP_GROUP,
            'kind': 'KubeadmControlPlane',
            'name': f'controlplane-{cluster.cluster_name}'
        }
    }
    manifest: config_util.YamlDict = {
        'apiVersion': updater.CAPI_GROUP,
        'kind': 'Cluster',
        'metadata': cluster.metadata(
            cluster.cluster_name,
            labels={'kanod.io/cluster_name': cluster.cluster_name}),
        'spec': spec
    }
    return manifest


def metal3_cluster_manifest(cluster: updater.Cluster) -> config_util.YamlDict:
    spec = {
        'controlPlaneEndpoint': {
            'host': cluster.endpoint_host,
            'port': cluster.endpoint_port
        },
        'noCloudProvider': False
    }
    bm_config = cast(
        config_util.YamlDict,
        cluster.infra_config.get('baremetal', {}))
    prefixes = cast(
        Optional[str], bm_config.get('dynamic-labels-prefix', None))
    if prefixes is not None:
        metal3cluster_annots: Optional[Dict[str, str]] = {
            'metal3.io/metal3-label-sync-prefixes': prefixes
        }
    else:
        metal3cluster_annots = None
    manifest: config_util.YamlDict = {
        'apiVersion': updater.CAPM3_GROUP,
        'kind': 'Metal3Cluster',
        'metadata': cluster.metadata(
            cluster.cluster_name, annotations=metal3cluster_annots),
        'spec': spec
    }
    return manifest
