#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import re
from typing import cast, Dict, List, Tuple, Optional, Union  # noqa: H301

import hashlib
import logging
import tempfile

from . import merge

from . import config_util
from . import validator
from . import vault

CAPI_GROUP = 'cluster.x-k8s.io/v1beta1'
CAPM3_GROUP = 'infrastructure.cluster.x-k8s.io/v1beta1'
BOOTSTRAP_GROUP = 'bootstrap.cluster.x-k8s.io/v1beta1'
CP_GROUP = 'controlplane.cluster.x-k8s.io/v1beta1'
BAREMETALPOOL_GROUP = 'bmp.kanod.io/v1'
NETPOOL_GROUP = 'netpool.kanod.io/v1'

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def build_uid(*s: str) -> str:
    d = hashlib.sha1()
    for e in s:
        d.update(e.encode('utf-8'))
    return d.hexdigest()[0:8]


def extract_templates(
    config: config_util.YamlDict, entry: str
) -> Dict[str, config_util.YamlDict]:
    pre_templates = cast(List[config_util.YamlDict], config.get(entry, []))
    result = {
        (cast(str, template['name'])): template
        for template in pre_templates
        if 'name' in template
    }
    for template in pre_templates:
        template.pop('name', None)
    return result


def find_flavour(
    name: str, flavours: List[config_util.YamlDict]
) -> Optional[config_util.YamlDict]:
    for flavour in flavours:
        if flavour.get('flavour', '') == name:
            return flavour
    return None


def cidata_replacer(cidata: Dict[str, str], target: str):
    def replacer(matchobj):
        return cidata.get(matchobj.group(1), matchobj.group(0))
    regex = r'{{[ ]*ds.meta_data.([a-zA-Z_0-9]+[ ]*)}}'
    return re.sub(regex, replacer, target)


class Cluster:

    def __init__(
        self,
        error_handler: config_util.ErrorHandler,
        cluster_name: str,
        infra_config: config_util.YamlDict,
        cluster_config: config_util.YamlDict,
        cidata: Dict[str, str] = {},
        namespace: Optional[str] = None,
        token: Optional[str] = None,
    ):
        self.error_handler = error_handler
        self.cluster_name = cluster_name
        self.namespace = namespace
        self.infra_config = infra_config
        self.cluster_config = cluster_config
        self.token = token
        self.errors: List[Tuple[str, str]] = []
        self.warnings: List[Tuple[str, str]] = []
        self.current_controlplane_version: Optional[str] = None
        self.machine_versions: List[str] = []
        self.limit_controlplane: Optional[int] = None
        self.limit_workers: Optional[int] = None

        validator.validate_infra(self.error_handler, infra_config)
        validator.validate_cluster(self.error_handler, cluster_config)
        # We do not try to go further as all the typing assumes the schema
        # are respected.
        self.error_handler.abort_on_errors()

        def specialize(
            template: config_util.YamlDict,
            flavours: List[config_util.YamlDict]
        ):
            flavourName = cast(str, template.get('flavour', 'default'))
            infra = find_flavour(flavourName, flavours)
            return merge.merge(self.error_handler, {}, infra, template)

        cp_flavours = cast(
            List[config_util.YamlDict],
            infra_config.get('controlPlaneFlavours', None))
        cluster_cp = cast(
            config_util.YamlDict,
            cluster_config.get('controlPlane', None))
        full_controlplane = specialize(cluster_cp, cp_flavours)
        self.k8sconfig = cast(
            config_util.YamlDict,
            full_controlplane.get('kubernetes', {}))
        endpoint = cast(
            config_util.YamlDict, self.k8sconfig.get('endPoint', {}))
        self.endpoint_host = cidata_replacer(
            cidata, cast(str, endpoint.get('host', '')))
        # TODO(pc) We should have a way to use meta data. This breaks
        # typing currently.
        self.endpoint_port = cast(int, endpoint.get('port', 6443))
        if self.endpoint_host == '':
            self.error_handler.error('configuration', 'No endpoint specified')
        self.controlplane = full_controlplane

        worker_flavours = cast(
            List[config_util.YamlDict],
            infra_config.get('workerFlavours', []))
        cluster_workers = cast(
            List[config_util.YamlDict],
            cluster_config.get('workers', []))
        self.workers = [
            specialize(worker, worker_flavours)
            for worker in cluster_workers
        ]

        baremetalpools = cast(
            List[config_util.YamlDict],
            cluster_config.get('baremetalpools', []))

        self.baremetalpools = baremetalpools

        networkpools = cast(
            List[config_util.YamlDict],
            cluster_config.get('networkpools', []))

        self.networkpools = networkpools

        cluster_cidata = cast(
            Dict[str, str], cluster_config.get('cidata', {}))
        infra_cidata = cast(
            Dict[str, str], infra_config.get('cidata', {}))
        self.cidata = {**cluster_cidata, **infra_cidata, **cidata}
        self.vault = vault.VaultHandler(
            self.cluster_name, self.error_handler,
            self.cluster_config.get("vault", {}))
        self.nexus = cast(config_util.YamlDict, infra_config.get('nexus', {}))
        self.maven = cast(Optional[str], self.nexus.get('maven', None))
        self.verify = self.get_maven_verify()
        self.docker = cast(Optional[str], self.nexus.get('docker', None))
        self.addons_templates = extract_templates(
            infra_config, 'addonFlavours')

    def get_maven_verify(self) -> Union[str, bool]:
        cert = cast(Optional[str], self.nexus.get('certificate', None))
        if cert is None:
            return True
        else:
            if cert == '@vault:ca':
                cert = self.vault.get_maven_ca()
            if cert is None:
                return True
            with tempfile.NamedTemporaryFile(
                mode='w', delete=False, encoding='utf-8'
            ) as fd:
                fd.write(cert)
                fd.write('\n')
                verify = fd.name
            return verify

    def metadata(
        self, name: str,
        labels: Optional[Dict[str, str]] = None,
        annotations: Optional[Dict[str, str]] = None
    ) -> config_util.YamlDict:
        mt: config_util.YamlDict = {'name': name}
        if self.namespace is not None:
            mt['namespace'] = self.namespace
        if labels is not None:
            mt['labels'] = labels
        if annotations is not None:
            mt['annotations'] = annotations
        return mt
