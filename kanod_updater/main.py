#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import json
import subprocess
import sys
from typing import cast, Dict, List, Tuple, Optional  # noqa: H301

import argparse
from kubernetes import client  # type: ignore
from kubernetes import config  # type: ignore
import logging
import os
import yaml

from . import baremetalpool
from . import cluster as cluster_manifest
from . import config_util
from . import data
from . import kubeadm
from . import machines
from . import networkpool
from . import resourceset
from . import updater
from . import versions

log = logging.getLogger(__name__)

ARGOCD_ENV_PREFIX = "ARGOCD_ENV_"


def read_yaml(name: str) -> config_util.YamlDict:
    with open(name, 'r') as fd:
        config = yaml.load(fd, Loader=yaml.Loader)
    return config


def read_yaml_string(spec: str) -> config_util.YamlDict:
    return yaml.load(spec, Loader=yaml.Loader)


def generate_output(cluster: updater.Cluster, output: str):
    log.info(f'cluster generation {cluster.cluster_name}')
    cluster_spec = cluster_manifest.cluster_manifest(cluster)
    log.info('Metal3 cluster generation')
    metal3_cluster = cluster_manifest.metal3_cluster_manifest(cluster)
    cp_spec = cast(Optional[config_util.YamlDict],
                   cluster.controlplane)
    if cp_spec is None:
        raise Exception('cluster must define a control-plane')
    log.info('Control plane')
    cp_version = versions.controlplane_version(cluster)
    if cp_version is None:
        cluster.error_handler.error(
            'validation',
            'Cannot proceed without Kubernetes version for'
            f'control-plane of {cluster.cluster_name}')
        return
    metadata_name, metadata = data.metadata_manifest(cluster)
    controlplane = kubeadm.kubeadm_controlplane_manifest(cluster, cp_version)
    log.info('Machine template for controlplane')
    cp_machine = machines.machine_template_manifest(
        cluster, None, cp_spec, metadata_name, cp_version)
    log.info('Metadata')
    resource_list = [
        cluster_spec, metal3_cluster, metadata, controlplane, cp_machine
    ]
    log.info('Machine deployments')
    for md in cast(List[config_util.YamlDict], cluster.workers):
        resource_list += kubeadm.full_deployment(cluster, md, metadata_name)
    resource_list += resourceset.generate_inlined_addons(cluster, cp_version)

    log.info('Baremetalpool resources')
    for bmpool in cast(List[config_util.YamlDict], cluster.baremetalpools):
        bm_resource = baremetalpool.baremetalpool_manifest(cluster, bmpool)
        resource_list += bm_resource

    log.info('Network resources')
    for netpool in cast(List[config_util.YamlDict], cluster.networkpools):
        network_resource = networkpool.networkpool_manifest(cluster, netpool)
        resource_list += network_resource

    log.info('Full dump')
    with config_util.extended_open(output) as fd:
        yaml.dump_all(resource_list, stream=fd, explicit_start=True)


def update(
    conf_path: str,
    cluster_path: str,
    cluster_name: str,
    cidata_path: str,
    namespace: Optional[str],
    output: Optional[str]
):
    error_handler = config_util.ErrorHandler()
    try:
        config_util.setup_yaml_formatter()
        config = read_yaml(conf_path)
        descr = read_yaml(cluster_path)
        cidata = {}
        if cidata_path != "":
            cidata = cast(Dict[str, str], read_yaml(cidata_path))
        cluster = updater.Cluster(
            error_handler,
            cluster_name, config, descr, cidata, namespace=namespace)
        cluster.error_handler.abort_on_errors()
        if output is not None:
            generate_output(cluster, output)
        cluster.error_handler.abort_on_errors()
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        exit(1)


def kubernetes_errors(
    namespace: str, cluster: updater.Cluster,
):
    '''Report errors or the lack of through annotations of ClusterDef

    :param namespace: the namespace of the clusterdef resource
    :param name: the name of the clusterdef resource
    :param error_handler: the handler containing the errors.
    '''
    name = cluster.cluster_name
    error_handler = cluster.error_handler
    cpbound = cluster.limit_controlplane
    wkbound = cluster.limit_workers
    api = client.CustomObjectsApi()

    def dump(list: List[Tuple[str, str]]) -> Optional[str]:
        return None if list == [] else json.dumps(list)
    api.patch_namespaced_custom_object(
        group='gitops.kanod.io',
        version="v1alpha1",
        namespace=namespace,
        plural="clusterdefs",
        name=name,
        body={
            "metadata": {
                "annotations": {
                    "kanod.io/errors": dump(error_handler.errors),
                    "kanod.io/warnings": dump(error_handler.warnings),
                    "kanod.io/bound-controlplane": (
                        None if cpbound is None else str(cpbound)),
                    "kanod.io/bound-workers": (
                        None if wkbound is None else str(wkbound))
                }}
        })


def update_in_cluster(
    cdef_ns: str,
    conf_ns: str,
    conf_name: str,
    cidata_name: str,
    cluster_path: str,
    cluster_name: str,
    namespace: Optional[str],
):
    error_handler = config_util.ErrorHandler()
    try:
        config_util.setup_yaml_formatter()
        descr = read_yaml(cluster_path)

        config.load_incluster_config()
        corev1 = client.CoreV1Api()
        cm = corev1.read_namespaced_config_map(conf_name, conf_ns)
        if cm is None:
            error_handler.error(
                'infra',
                f'Cannot find infrastructure configmap for {cluster_name}')
            raise config_util.UpdaterException()
        infra_text = cm.data.get('config', None)
        if infra_text is None:
            error_handler.error(
                'infra', 'config is not defined in infra configmap'
                f'for cluster {cluster_name}')
            raise config_util.UpdaterException()

        infra = read_yaml_string(infra_text)
        if cidata_name == "":
            log.info(f'No specific cidata found for {cluster_name}')
            cidata = {}
        else:
            cm_cidata = corev1.read_namespaced_config_map(cidata_name, conf_ns)
            cidata = cm_cidata.data
        cluster = updater.Cluster(
            error_handler,
            cluster_name, infra, descr, cidata, namespace=namespace)
        versions.get_current_k8s_version(cdef_ns, cluster)
        cluster.error_handler.abort_on_errors()
        generate_output(cluster, '-')
        cluster.error_handler.abort_on_errors()
        kubernetes_errors(cdef_ns, cluster)
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        kubernetes_errors(cdef_ns, cluster)
        exit(1)


def vault_kustomize():
    os.environ['SSL_CERT_DIR'] = '/etc/ssl/certs:/app/config/tls'
    print(os.environ, file=sys.stderr)
    proc1 = subprocess.Popen(
        ['kustomize', 'build', '.'],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
    proc2 = subprocess.Popen(
        ['argocd-vault-plugin', 'generate', '-'],
        stdin=proc1.stdout, stderr=subprocess.PIPE, encoding='utf-8')
    proc1.stdout.close()
    ret1 = proc1.wait()
    if ret1 != 0:
        errs = proc1.communicate()[1]
        print(
            'kustomize failed: %d %s' % (ret1, errs),
            file=sys.stderr)
    ret2 = proc2.wait()
    if ret2 != 0:
        errs = proc2.communicate()[1]
        print(
            'vault plugin failed: %d %s' % (ret2, errs),
            file=sys.stderr)
        exit(1)
    print('Successful kustomize / vault plugin run', file=sys.stderr)
    exit(0)


def argocd_get(key, default):
    return os.environ.get(
        ARGOCD_ENV_PREFIX + key, os.environ.get(key, default))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config",
        help="Configuration file for infra requirements"
    )
    parser.add_argument(
        "-d", "--discover", action=argparse.BooleanOptionalAction,
        help="Discover mode for argocd plugin"
    )
    parser.add_argument(
        "-k", "--kubernetes", action=argparse.BooleanOptionalAction,
        help="Parse for kubernetes mode from environment variables"
    )
    parser.add_argument(
        "-C", "--configmap",
        help="Configuration configmap name for infra requirements"
    )
    parser.add_argument(
        "-o", "--output",
        help="Output file for cluster-api (YAML)"
    )
    parser.add_argument(
        '-e', '--cdefns',
        help='Specify clusterdef namespace'
    )
    parser.add_argument(
        '-n', '--namespace',
        help='Specify output namespace'
    )
    parser.add_argument(
        "-N", "--cmns",
        help="Configuration configmap namespace for infra requirements"
    )
    parser.add_argument(
        "-m", "--cidata",
        help="Cloud-init metadata path to file or configmap name"
    )
    parser.add_argument(
        "-l", "--clustername", help="cluster name"
    )
    parser.add_argument(
        "-p", "--cluster", help="cluster description"
    )
    args = parser.parse_args()
    if args.discover:
        # No way to do something sensible without plugin variables.
        if not os.path.exists('Chart.yaml'):
            # At least we do not handle the repo if it is an Helm application.
            print("OK")
        exit(0)
    elif args.kubernetes:
        mode = argocd_get('PLUGIN_MODE', '')
        if mode == 'vault':
            cmd = subprocess.run(
                ['argocd-vault-plugin', 'generate', './'],
                stderr=subprocess.PIPE, encoding='utf-8')
            if cmd.returncode != 0:
                print(
                    ('vault plugin failed: %d %s' %
                     (cmd.returncode, cmd.stderr)),
                    file=sys.stderr)
                exit(1)
        elif mode == 'vault-kustomize':
            vault_kustomize()
        elif mode == 'updater':
            update_in_cluster(
                cdef_ns=argocd_get('CDEF_NAMESPACE', ''),
                conf_ns=argocd_get('INFRA_NAMESPACE', ''),
                conf_name=argocd_get('INFRA_CONFIGMAP', ''),
                cidata_name=argocd_get('CIDATA_CONFIGMAP', ''),
                cluster_path=argocd_get('CLUSTER_CONFIG_FILE', ''),
                cluster_name=argocd_get('CLUSTER_NAME', ''),
                namespace=argocd_get('TARGET_NAMESPACE', '')
            )
        else:
            if os.path.exists('kustomization.yaml'):
                os.system('kustomize build .')
            else:
                os.system(
                    "find . \\( -name '*.yaml' -o -name '*.yml' \\) "
                    "-exec cat {} \\; -exec echo '---' \\;")
    elif args.configmap is not None:
        update_in_cluster(
            cdef_ns=args.cdefns, conf_ns=args.cmns, conf_name=args.configmap,
            cidata_name=args.cidata, cluster_path=args.cluster,
            cluster_name=args.clustername, namespace=args.namespace)
    else:
        update(
            conf_path=args.config, cluster_path=args.cluster,
            cidata_path=args.cidata, cluster_name=args.clustername,
            namespace=args.namespace, output=args.output)
