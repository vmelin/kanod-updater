#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Any, List, Tuple  # noqa: H301

import yaml

from . import config_util
from . import updater


def metadata_manifest(
    cluster: updater.Cluster
) -> Tuple[str, config_util.YamlObj]:

    '''Builds the metal3DataTemplate

    This resource brings together various information to provide cloud-init
    meta-data to the nodes. This is used as an information channel between
    baremetal hosts, machines, ipam, updater configmaps and the servers.
    '''
    baremetal = cast(
        config_util.YamlDict, cluster.infra_config.get('baremetal', {}))
    labels = cast(List[str], baremetal.get('labels', []))
    if cluster.vault.use_tpm():
        labels.append('tpm-role-id')
    annotations = cast(List[str], baremetal.get('annotations', []))
    from_labels = [
        {
            'key': key.replace('-', '_'), 'object': 'baremetalhost',
            'label': f'kanod.io/{key}'
        }
        for key in labels
    ]
    from_annotations = [
        {
            'key': key.replace('-', '_'), 'object': 'baremetalhost',
            'annotation': f'kanod.io/{key}'
        }
        for key in annotations
    ]
    string_specs = [
        {'key': 'endpoint_host', 'value': cluster.endpoint_host},
        {'key': 'endpoint_port', 'value': str(cluster.endpoint_port)}
    ] + [
        {'key': key, 'value': value} for (key, value) in cluster.cidata.items()
    ]
    spec: Any = {
        'clusterName': cluster.cluster_name,
        'metaData': {
            'strings': string_specs,
        }
    }
    if len(from_labels) > 0:
        spec['metaData']['fromLabels'] = from_labels
    if len(from_annotations) > 0:
        spec['metaData']['fromAnnotations'] = from_annotations
    uid = updater.build_uid(yaml.dump(spec))
    name = f'{cluster.cluster_name}-metadata-{uid}'
    manifest: config_util.YamlDict = {
        'apiVersion': updater.CAPM3_GROUP,
        'kind': 'Metal3DataTemplate',
        'metadata': cluster.metadata(name),
        'spec': spec
    }
    return (name, manifest)
